module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig(
        {
            pkg: grunt.file.readJSON('package.json'),

            watch: {
                sass: {
                    files: ['<%= pkg.theme %>/sass/**/*.scss'],
                    tasks: ['sass:project', 'concat:css']
                }
            },

            sass: {
                project: {
                    options: {
                        style: 'expanded'
                    },
                    files: [{
                        expand: true,
                        cwd: '<%= pkg.theme %>/sass/',
                        src: ['*.scss'],
                        dest: '<%= pkg.theme %>/<%= pkg.cssDir %>/',
                        ext: '.css'
                    }]
                }
            },

            concat: {
                options: {
                    separator: '\n'
                },
                css: {
                    src: [
                        '<%= pkg.bowerDir %>/normalize-css/normalize.css',
                        '<%= pkg.theme %>/<%= pkg.cssDir %>/screen.css'
                    ],
                    dest: '<%= pkg.theme %>/<%= pkg.cssDir %>/main.css'
                }
            },
            connect: {
                server: {
                    options: {
                        port: 8080,
                        base: 'webroot',
                        open: true,
                        livereload: true
                    }
                }
            }
        }
    );

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');


    // Default task(s).

    grunt.registerTask('local', ['sass:project', 'concat', 'connect', 'watch']);

};