
## Check the Test
 
Access the Webroot and open the index.html in the browser 


## Requirements Develop

- Ruby (via RVM, tested with version >= 2.2.1p85)
- Ruby Gem Sass
- Node (via NVM, tested with version >= 0.10.40)

## Installation Env

The current project uses dependencies from both Node and Bower:

    $ npm install
   
## Working

You can simply run it from the root of the project Grunt local and It will execute several tasks as sass, concat css, watch for changes and create a server:

    $ grunt local
